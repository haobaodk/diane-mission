package com.paging.mission;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PagingMissionControlProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(PagingMissionControlProjectApplication.class, args);
	}

}
